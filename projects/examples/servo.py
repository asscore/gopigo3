# -*- coding: utf-8 -*-
#
# Przykład sterowania pozycją serwomechanizmu.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

gpg3 = gopigo3.GoPiGo3()


def degrees_to_pulse(degrees):
    return int((1375 - (gpg3.SERVO_PULSE_WIDTH_RANGE / 2)) + ((gpg3.SERVO_PULSE_WIDTH_RANGE / 180) * degrees))


try:
    gpg3.set_servo(gpg3.SERVO_1, degrees_to_pulse(45))
    time.sleep(1.)
    gpg3.set_servo(gpg3.SERVO_1, degrees_to_pulse(135))
    time.sleep(1.)
    gpg3.set_servo(gpg3.SERVO_1, degrees_to_pulse(90))
    time.sleep(1.)
except KeyboardInterrupt:
    gpg3.reset()
