# -*- coding: utf-8 -*-
#
# Test modułu z odbiornikiem podczerwieni (Grove - Infrared Receiver).
#  http://wiki.seeedstudio.com/Grove-Infrared_Receiver/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

gpg3 = gopigo3.GoPiGo3()
gpg3.set_grove_type(gpg3.GROVE_AD1, gpg3.GROVE_TYPE.IR_DI_REMOTE)

while True:
    try:
        print(gpg3.get_grove_value(gpg3.GROVE_AD1))
        time.sleep(.05)
    except KeyboardInterrupt:
        break
    except gopigo3.SensorError as error:
        print(error)

gpg3.set_grove_type(gpg3.GROVE_AD1, gpg3.GROVE_TYPE.CUSTOM)
