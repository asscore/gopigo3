# -*- coding: utf-8 -*-
#
# Test modułu z diodą LED (Grove - LED).
#  http://wiki.seeedstudio.com/Grove-LED_Socket_Kit/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

gpg3 = gopigo3.GoPiGo3()
gpg3.set_grove_type(gpg3.GROVE_AD1, gpg3.GROVE_TYPE.CUSTOM)
gpg3.set_grove_mode(gpg3.GROVE_AD1_1, gpg3.GROVE_MODE.OUTPUT_DIGITAL)

while True:
    try:
        gpg3.set_grove_state(gpg3.GROVE_AD1_1, gpg3.HIGH)
        time.sleep(1.)
        gpg3.set_grove_state(gpg3.GROVE_AD1_1, gpg3.LOW)
        time.sleep(1.)
    except KeyboardInterrupt:
        gpg3.set_grove_state(gpg3.GROVE_AD1_1, gpg3.LOW)
        break

gpg3.set_grove_mode(gpg3.GROVE_AD1_1, gpg3.GROVE_MODE.INPUT_DIGITAL)
