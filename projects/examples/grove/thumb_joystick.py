# -*- coding: utf-8 -*-
#
# Test modułu z joystickiem (Grove - Thumb Joystick).
#  http://wiki.seeedstudio.com/Grove-Thumb_Joystick/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

gpg3 = gopigo3.GoPiGo3()
gpg3.set_grove_type(gpg3.GROVE_AD1, gpg3.GROVE_TYPE.CUSTOM)
gpg3.set_grove_mode(gpg3.GROVE_AD1, gpg3.GROVE_MODE.INPUT_ANALOG)

while True:
    try:
        x = gpg3.get_grove_analog(gpg3.GROVE_AD1_1)
        y = gpg3.get_grove_analog(gpg3.GROVE_AD1_2)
        click = 1 if x >= 4070 else 0
        print('X: %d, Y: %d, Click: %d' % (x, y, click))
        time.sleep(.05)
    except KeyboardInterrupt:
        break

gpg3.set_grove_mode(gpg3.GROVE_AD1, gpg3.GROVE_MODE.INPUT_DIGITAL)
