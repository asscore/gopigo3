# -*- coding: utf-8 -*-
#
# Test modułu z przyciskiem (Grove - Button).
#  http://wiki.seeedstudio.com/Grove-Button/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

gpg3 = gopigo3.GoPiGo3()
gpg3.set_grove_type(gpg3.GROVE_AD1, gpg3.GROVE_TYPE.CUSTOM)
gpg3.set_grove_mode(gpg3.GROVE_AD1_1, gpg3.GROVE_MODE.INPUT_DIGITAL)

while True:
    try:
        if gpg3.get_grove_state(gpg3.GROVE_AD1_1) == gpg3.HIGH:
            print('On')
        else:
            print('Off')
        time.sleep(.5)
    except KeyboardInterrupt:
        break
