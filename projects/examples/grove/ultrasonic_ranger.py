# -*- coding: utf-8 -*-
#
# Test modułu z ultradźwiękowym czujnikiem odległości (Grove - Ultrasonic Ranger).
#  http://wiki.seeedstudio.com/Grove-Ultrasonic_Ranger/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

gpg3 = gopigo3.GoPiGo3()
gpg3.set_grove_type(gpg3.GROVE_AD1, gpg3.GROVE_TYPE.US)

while True:
    try:
        value = float(gpg3.get_grove_value(gpg3.GROVE_AD1))
        print('Distance: %d mm' % value)
        time.sleep(.05)
    except KeyboardInterrupt:
        break
    except gopigo3.SensorError as error:
        print(error)
    except gopigo3.ValueError as error:
        print(error)

gpg3.set_grove_type(gpg3.GROVE_AD1, gpg3.GROVE_TYPE.CUSTOM)
