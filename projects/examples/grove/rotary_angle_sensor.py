# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem obrotowym (Grove - Rotary Angle Sensor).
#  http://wiki.seeedstudio.com/Grove-Rotary_Angle_Sensor/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

FULL_ANGLE = 300

gpg3 = gopigo3.GoPiGo3()
gpg3.set_grove_type(gpg3.GROVE_AD1, gpg3.GROVE_TYPE.CUSTOM)
gpg3.set_grove_mode(gpg3.GROVE_AD1_1, gpg3.GROVE_MODE.INPUT_ANALOG)

grove_vcc = gpg3.get_voltage_5v()

while True:
    try:
        voltage = gpg3.get_grove_voltage(gpg3.GROVE_AD1_1)
        degrees = (voltage * FULL_ANGLE) / grove_vcc
        print('Voltage: %.2f Degrees: %.1f' % (voltage, degrees))
        time.sleep(.5)
    except KeyboardInterrupt:
        break

gpg3.set_grove_mode(gpg3.GROVE_AD1_1, gpg3.GROVE_MODE.INPUT_DIGITAL)
