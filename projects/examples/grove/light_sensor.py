# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem światła (Grove - Light Sensor).
#  http://wiki.seeedstudio.com/Grove-Light_Sensor/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

gpg3 = gopigo3.GoPiGo3()
gpg3.set_grove_type(gpg3.GROVE_AD1, gpg3.GROVE_TYPE.CUSTOM)
gpg3.set_grove_mode(gpg3.GROVE_AD1_1, gpg3.GROVE_MODE.INPUT_ANALOG)

while True:
    try:
        sensor_value = gpg3.get_grove_analog(gpg3.GROVE_AD1_1)
        percent = sensor_value * 100 / 4095
        print('Sensor value: %d (%.2f %%)' % (sensor_value, percent))
        time.sleep(.05)
    except KeyboardInterrupt:
        break

gpg3.set_grove_mode(gpg3.GROVE_AD1_1, gpg3.GROVE_MODE.INPUT_DIGITAL)
