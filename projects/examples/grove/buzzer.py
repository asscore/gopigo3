# -*- coding: utf-8 -*-
#
# Test modułu z buzzerem (Grove - Buzzer).
#  http://wiki.seeedstudio.com/Grove-Buzzer/
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

scale = [261.63, 293.66, 329.63, 349.23, 392.00, 440.00, 493.88, 523.25]

gpg3 = gopigo3.GoPiGo3()
gpg3.set_grove_type(gpg3.GROVE_AD1, gpg3.GROVE_TYPE.CUSTOM)
gpg3.set_grove_mode(gpg3.GROVE_AD1_1, gpg3.GROVE_MODE.OUTPUT_PWM)

gpg3.set_grove_pwm_duty(gpg3.GROVE_AD1_1, 20)

note = 0
while True:
    try:
        gpg3.set_grove_pwm_frequency(gpg3.GROVE_AD1, scale[note])
        note += 1
        if note >= 8:
            note = 0
        time.sleep(.5)
    except KeyboardInterrupt:
        break

gpg3.set_grove_mode(gpg3.GROVE_AD1_1, gpg3.GROVE_MODE.INPUT_DIGITAL)
