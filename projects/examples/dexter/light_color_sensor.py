# -*- coding: utf-8 -*-
#
# Test modułu z czujnikiem koloru.
#  Dexter Industries Light & Color Sensor
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import dexter

lc = dexter.LightColor(led_state=True)

while True:
    try:
        red, green, blue, clear = lc.read_raw_color()
        print('Color: red = %d green = %d blue = %d clear = %d' % (red, green, blue, clear))
        time.sleep(.02)
    except KeyboardInterrupt:
        lc.set_led(False)
        break
