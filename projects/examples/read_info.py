# -*- coding: utf-8 -*-
#
# Odczyt informacji o nakładce GoPiGo3.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import gopigo3

try:
    gpg3 = gopigo3.GoPiGo3()

    print('Manufacturer    : %s' % gpg3.get_manufacturer())
    print('Board           : %s' % gpg3.get_board())
    print('Serial Number   : %s' % gpg3.get_id())
    print('Hardware version: %s' % gpg3.get_version_hardware())
    print('Firmware version: %s' % gpg3.get_version_firmware())
    print('Battery voltage : %.02f V' % gpg3.get_voltage_battery())
    print('5v voltage      : %.03f V' % gpg3.get_voltage_5v())
except IOError as error:
    print(error)
except gopigo3.FirmwareVersionError as error:
    print(error)
