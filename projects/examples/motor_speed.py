# -*- coding: utf-8 -*-
#
# Przykład sterowania prędkością silników.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

gpg3 = gopigo3.GoPiGo3()

try:
    gpg3.offset_motor_encoder(gpg3.MOTOR_LEFT, gpg3.get_motor_encoder(gpg3.MOTOR_LEFT))
    gpg3.offset_motor_encoder(gpg3.MOTOR_RIGHT, gpg3.get_motor_encoder(gpg3.MOTOR_RIGHT))
    while True:
        encoder_left = gpg3.get_motor_encoder(gpg3.MOTOR_LEFT)
        print('Motor Left Encoder: %6d' % encoder_left, '  Motor Right status: ',
              gpg3.get_motor_status(gpg3.MOTOR_RIGHT))
        gpg3.set_motor_dps(gpg3.MOTOR_RIGHT, encoder_left)
        time.sleep(.02)
except KeyboardInterrupt:
    gpg3.reset()
