# -*- coding: utf-8 -*-
#
# Przykład sterowania pozycją silników.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

gpg3 = gopigo3.GoPiGo3()

try:
    gpg3.offset_motor_encoder(gpg3.MOTOR_LEFT, gpg3.get_motor_encoder(gpg3.MOTOR_LEFT))
    gpg3.offset_motor_encoder(gpg3.MOTOR_RIGHT, gpg3.get_motor_encoder(gpg3.MOTOR_RIGHT))
    for i in range(0, 361):
        gpg3.set_motor_position(gpg3.MOTOR_LEFT + gpg3.MOTOR_RIGHT, i)
        time.sleep(.01)
    while True:
        for i in range(-360, 361):
            gpg3.set_motor_position(gpg3.MOTOR_LEFT + gpg3.MOTOR_RIGHT, -i)
            time.sleep(.01)
        for i in range(-360, 361):
            gpg3.set_motor_position(gpg3.MOTOR_LEFT + gpg3.MOTOR_RIGHT, i)
            time.sleep(.01)
except KeyboardInterrupt:
    gpg3.reset()
