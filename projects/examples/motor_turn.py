# -*- coding: utf-8 -*-
#
# Obrót względem prawego/lewego koła o zadaną liczbę stopni.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

gpg3 = gopigo3.GoPiGo3()


def turn_degrees(degrees, speed):
    start_position_left = gpg3.get_motor_encoder(gpg3.MOTOR_LEFT)
    start_position_right = gpg3.get_motor_encoder(gpg3.MOTOR_RIGHT)

    wheel_travel_distance = ((gpg3.WHEEL_BASE_CIRCUMFERENCE * degrees) / 360)
    wheel_turn_degrees = ((wheel_travel_distance / gpg3.WHEEL_CIRCUMFERENCE) * 360)

    gpg3.set_motor_limits(gpg3.MOTOR_LEFT + gpg3.MOTOR_RIGHT, dps=speed)

    gpg3.set_motor_position(gpg3.MOTOR_LEFT, (start_position_left + wheel_turn_degrees))
    gpg3.set_motor_position(gpg3.MOTOR_RIGHT, (start_position_right - wheel_turn_degrees))


try:
    turn_degrees(90, 100)
    time.sleep(7.)
    turn_degrees(-180, 100)
    time.sleep(8.)
    turn_degrees(90, 100)
    time.sleep(3.)
except KeyboardInterrupt:
    pass

gpg3.reset()
