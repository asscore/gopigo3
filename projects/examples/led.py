# -*- coding: utf-8 -*-
#
# Przykład sterowania diodami LED.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

gpg3 = gopigo3.GoPiGo3()

try:
    while True:
        for i in range(11):
            gpg3.set_led(gpg3.LED_EYE_LEFT, i, i, i)
            gpg3.set_led(gpg3.LED_EYE_RIGHT, 10 - i, 10 - i, 10 - i)
            gpg3.set_led(gpg3.LED_LEFT, (i * 25))
            gpg3.set_led(gpg3.LED_RIGHT, ((10 - i) * 25))
            time.sleep(.02)

        gpg3.set_led(gpg3.LED_WIFI, 0, 0, 10)

        for i in range(11):
            gpg3.set_led(gpg3.LED_EYE_LEFT, 10 - i, 10 - i, 10 - i)
            gpg3.set_led(gpg3.LED_EYE_RIGHT, i, i, i)
            gpg3.set_led(gpg3.LED_LEFT, ((10 - i) * 25))
            gpg3.set_led(gpg3.LED_RIGHT, (i * 25))
            time.sleep(.02)

        gpg3.set_led(gpg3.LED_WIFI, 0, 0, 0)
except KeyboardInterrupt:
    gpg3.reset()
