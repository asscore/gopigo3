# -*- coding: utf-8 -*-
#
# Przykład odczytu pozycji enkoderów.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import gopigo3

gpg3 = gopigo3.GoPiGo3()

try:
    gpg3.offset_motor_encoder(gpg3.MOTOR_LEFT, gpg3.get_motor_encoder(gpg3.MOTOR_LEFT))
    gpg3.offset_motor_encoder(gpg3.MOTOR_RIGHT, gpg3.get_motor_encoder(gpg3.MOTOR_RIGHT))
    while True:
        print('Encoder L: %6d R: %6d' % \
              (gpg3.get_motor_encoder(gpg3.MOTOR_LEFT), gpg3.get_motor_encoder(gpg3.MOTOR_RIGHT)))
        time.sleep(.025)
except KeyboardInterrupt:
    gpg3.reset()
