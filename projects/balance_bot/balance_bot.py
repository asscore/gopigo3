# -*- coding: utf-8 -*-
#
# Robot balansujący.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import sys
import time

import gopigo3
from modules import dexter

gpg3 = gopigo3.GoPiGo3()
gpg3.reset()

imu = dexter.IMU(bus='AD1')

DRIVE_SPEED = 175  # Prędkość w stopnie/s
STEER_SPEED = 100  # Szybkość sterowania w stopnie/s

# Stałe używane do określenia, jak agresywnie robot powinien reagować na:
KGYROANGLE = 15  # Nachylenie (kąt)
KGYROSPEED = 1.7  # Zmiany kąta
KPOS = .07  # Odchylenie od pozycji docelowej
KSPEED = .1  # Rzeczywista prędkość 
KDRIVE = -.02  # Docelowa prędkość
KSTEER = .25  # Błąd w sterowaniu
KVOLTAGE = .083  # Zmiany napięcia

LOOP_SPEED = 100  # Szybkość pętli balansującej w Hz
LOOP_TIME = (1 / LOOP_SPEED)  # Czas trwania pętli w s

TIME_FALL_LIMIT = 2  # Czas w s po którym należy przyjąć, że robot upadł

KCORRECTTIME = .001  # Korekta opóźnienia
KGYROANGLECORRECT = .1  # Korekta nachylenia (kąta)

WHEEL_DIAMETER = gpg3.WHEEL_DIAMETER  # Średnica koła w mm
WHEEL_RATIO = (WHEEL_DIAMETER / 56)  # Dostosowanie do kół 56 mm


def safe_exit():
    gpg3.reset()
    sys.exit()


def ready_for_balance():
    gpg3.set_motor_power(gpg3.MOTOR_LEFT + gpg3.MOTOR_RIGHT, gpg3.MOTOR_FLOAT)

    global gyro_angle
    global mrc_sum
    global motor_pos
    global mrc_delta
    global mrc_delta_p1
    global mrc_delta_p2
    global mrc_delta_p3
    global motor_diff_target
    global time_offset
    global t_interval
    global last_time

    print('Stand robot up and then press OK on the remote.')
    while gpg3.get_grove_value(gpg3.GROVE_AD2) != 3:
        time.sleep(.1)

    # Wyzerowanie pozycji enkoderów
    gpg3.offset_motor_encoder(gpg3.MOTOR_LEFT, gpg3.get_motor_encoder(gpg3.MOTOR_LEFT))
    gpg3.offset_motor_encoder(gpg3.MOTOR_RIGHT, gpg3.get_motor_encoder(gpg3.MOTOR_RIGHT))

    gyro_angle = 0
    mrc_sum = 0
    motor_pos = 0
    mrc_delta = 0
    mrc_delta_p1 = 0
    mrc_delta_p2 = 0
    mrc_delta_p3 = 0
    motor_diff_target = 0
    time_offset = 0
    t_interval = LOOP_TIME
    last_time = time.time() - LOOP_TIME

    print('Balancing, so let go of the robot.')
    print('Use Up, Down, Left, and Right on the remote to drive the robot.')


try:
    print('GoPiGo3 BalanceBot.')

    if gpg3.get_voltage_battery() < 9:
        print('Battery voltage below 9v, too low to run reliably. Exiting.')
        safe_exit()

    gpg3.set_grove_type(gpg3.GROVE_AD2, gpg3.GROVE_TYPE.IR_DI_REMOTE)

    ready_for_balance()

    t_motor_pos_ok = time.time()

    while True:
        try:
            # Utrzymanie stałej prędkości pętli balansującej 
            current_time = time.time()
            time_offset += ((t_interval - LOOP_TIME) * KCORRECTTIME)
            delay_time = (LOOP_TIME - (current_time - last_time)) - time_offset
            if delay_time > 0:
                time.sleep(delay_time)
            current_time = time.time()
            t_interval = (current_time - last_time)
            last_time = current_time

            button = gpg3.get_grove_value(gpg3.GROVE_AD2)

            if button == 1:
                motor_control_drive = DRIVE_SPEED
                motor_control_steer = 0
            elif button == 5:
                motor_control_drive = -DRIVE_SPEED
                motor_control_steer = 0
            elif button == 2:
                motor_control_drive = 0
                motor_control_steer = -STEER_SPEED
            elif button == 4:
                motor_control_drive = 0
                motor_control_steer = STEER_SPEED
            else:
                motor_control_drive = 0
                motor_control_steer = 0

            # Odczytanie prędkości kątowej (oś Z)
            gyro_speed = imu.read_gyroscope()[2]

            # Nachylenie (kąt)
            gyro_angle += gyro_speed * t_interval

            # Korekta nachylenia
            gyro_angle -= (gyro_angle * KGYROANGLECORRECT * t_interval)

            # Odczytanie bieżącej pozycji enkoderów
            mrc_left = gpg3.get_motor_encoder(gpg3.MOTOR_LEFT)
            mrc_right = gpg3.get_motor_encoder(gpg3.MOTOR_RIGHT)

            # Obliczenie prędkości
            mrc_sum_prev = mrc_sum
            mrc_sum = mrc_left + mrc_right
            mrc_delta = mrc_sum - mrc_sum_prev
            motor_speed = mrc_delta / t_interval

            # Obliczenie pozycji
            motor_pos += (mrc_delta - (motor_control_drive * t_interval))

            # Obliczenie mocy
            power = (((KGYROSPEED * gyro_speed + KGYROANGLE * gyro_angle) / WHEEL_RATIO + KPOS * motor_pos +
                      KDRIVE * motor_control_drive + KSPEED * motor_speed) / (KVOLTAGE * gpg3.get_voltage_battery()))

            if abs(power) < 100:
                t_motor_pos_ok = current_time

            # Obliczenie mocy silników
            motor_diff_target += motor_control_steer * t_interval
            motor_diff = mrc_left - mrc_right
            power_steer = KSTEER * (motor_diff_target - motor_diff)
            power_left = power + power_steer
            power_right = power - power_steer

            if power_left > 100:
                power_left = 100
            if power_left < -100:
                power_left = -100
            if power_right > 100:
                power_right = 100
            if power_right < -100:
                power_right = -100

            # Ustawienie mocy silników
            gpg3.set_motor_power(gpg3.MOTOR_LEFT, power_left)
            gpg3.set_motor_power(gpg3.MOTOR_RIGHT, power_right)

            # Sprawdzenie, czy robot nie upadł
            if (current_time - t_motor_pos_ok) > TIME_FALL_LIMIT:
                print('Robot fell.')
                ready_for_balance()

        except gopigo3.SensorError as error:
            print(error)
        except IOError as error:
            print(error)

except KeyboardInterrupt:
    print('\nCtrl+C. Exiting.')
    safe_exit()
except Exception:
    print('Exception. Exiting.')
    safe_exit()
