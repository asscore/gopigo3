# -*- coding: utf-8 -*-
#
# BME280 - czujnik wilgotności, ciśnienia i temperatury.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

import i2c

# Rejestry
REG_DIG_T1 = 0x88
REG_DIG_T2 = 0x8A
REG_DIG_T3 = 0x8C

REG_DIG_P1 = 0x8E
REG_DIG_P2 = 0x90
REG_DIG_P3 = 0x92
REG_DIG_P4 = 0x94
REG_DIG_P5 = 0x96
REG_DIG_P6 = 0x98
REG_DIG_P7 = 0x9A
REG_DIG_P8 = 0x9C
REG_DIG_P9 = 0x9E

REG_DIG_H1 = 0xA1
REG_DIG_H2 = 0xE1
REG_DIG_H3 = 0xE3
REG_DIG_H4 = 0xE4
REG_DIG_H5 = 0xE5
REG_DIG_H6 = 0xE6
REG_DIG_H7 = 0xE7

REG_CHIPID = 0xD0
REG_VERSION = 0xD1
REG_SOFTRESET = 0xE0

REG_STATUS = 0xF3
REG_CONTROL_HUM = 0xF2
REG_CONTROL = 0xF4
REG_CONFIG = 0xF5
REG_PRESS_DATA = 0xF7
REG_TEMP_DATA = 0xFA
REG_HUM_DATA = 0xFD

# Tryb pomiaru
OSAMPLE_1 = 1
OSAMPLE_2 = 2
OSAMPLE_4 = 3
OSAMPLE_8 = 4
OSAMPLE_16 = 5

# Odstęp czasowy pomiędzy odczytami
STANDBY_0P5 = 0
STANDBY_62P5 = 1
STANDBY_125 = 2
STANDBY_250 = 3
STANDBY_500 = 4
STANDBY_1000 = 5
STANDBY_10 = 6
STANDBY_20 = 7

# Filtr
FILTER_OFF = 0
FILTER_2 = 1
FILTER_4 = 2
FILTER_8 = 3
FILTER_16 = 4


class BME280(object):
    def __init__(self, bus='I2C', address=0x76, t_mode=OSAMPLE_1, p_mode=OSAMPLE_1, h_mode=OSAMPLE_1,
                 standby=STANDBY_250, filter=FILTER_OFF):
        if t_mode not in [OSAMPLE_1, OSAMPLE_2, OSAMPLE_4, OSAMPLE_8, OSAMPLE_16]:
            raise ValueError('Unexpected t_mode %d.' % t_mode)

        if p_mode not in [OSAMPLE_1, OSAMPLE_2, OSAMPLE_4, OSAMPLE_8, OSAMPLE_16]:
            raise ValueError('Unexpected p_mode %d.' % p_mode)

        if h_mode not in [OSAMPLE_1, OSAMPLE_2, OSAMPLE_4, OSAMPLE_8, OSAMPLE_16]:
            raise ValueError('Unexpected h_mode %d.' % h_mode)

        if standby not in [STANDBY_0P5, STANDBY_62P5, STANDBY_125, STANDBY_250, STANDBY_500, STANDBY_1000, STANDBY_10,
                           STANDBY_20]:
            raise ValueError('Unexpected standby value %d.' % standby)

        if filter not in [FILTER_OFF, FILTER_2, FILTER_4, FILTER_8, FILTER_16]:
            raise ValueError('Unexpected filter value %d.' % filter)

        self.i2c_bus = i2c.Bus(address, big_endian=False, bus=bus)
        self._load_calibration()
        self.i2c_bus.write_reg_8(REG_CONTROL, 0x24)
        time.sleep(.002)
        self.i2c_bus.write_reg_8(REG_CONFIG, ((standby << 5) | (filter << 2)))
        time.sleep(.002)
        self.i2c_bus.write_reg_8(REG_CONTROL_HUM, h_mode)
        self.i2c_bus.write_reg_8(REG_CONTROL, ((t_mode << 5) | (p_mode << 2) | 3))
        self.t_fine = .0

    def _load_calibration(self):
        self.dig_t1 = self.i2c_bus.read_reg_16u(REG_DIG_T1)
        self.dig_t2 = self.i2c_bus.read_reg_16s(REG_DIG_T2)
        self.dig_t3 = self.i2c_bus.read_reg_16s(REG_DIG_T3)

        self.dig_p1 = self.i2c_bus.read_reg_16u(REG_DIG_P1)
        self.dig_p2 = self.i2c_bus.read_reg_16s(REG_DIG_P2)
        self.dig_p3 = self.i2c_bus.read_reg_16s(REG_DIG_P3)
        self.dig_p4 = self.i2c_bus.read_reg_16s(REG_DIG_P4)
        self.dig_p5 = self.i2c_bus.read_reg_16s(REG_DIG_P5)
        self.dig_p6 = self.i2c_bus.read_reg_16s(REG_DIG_P6)
        self.dig_p7 = self.i2c_bus.read_reg_16s(REG_DIG_P7)
        self.dig_p8 = self.i2c_bus.read_reg_16s(REG_DIG_P8)
        self.dig_p9 = self.i2c_bus.read_reg_16s(REG_DIG_P9)

        self.dig_h1 = self.i2c_bus.read_reg_8u(REG_DIG_H1)
        self.dig_h2 = self.i2c_bus.read_reg_16s(REG_DIG_H2)
        self.dig_h3 = self.i2c_bus.read_reg_8u(REG_DIG_H3)
        self.dig_h6 = self.i2c_bus.read_reg_8s(REG_DIG_H7)

        h4 = self.i2c_bus.read_reg_8s(REG_DIG_H4)
        h4 = (h4 << 4)
        self.dig_h4 = h4 | (self.i2c_bus.read_reg_8u(REG_DIG_H5) & 0x0F)

        h5 = self.i2c_bus.read_reg_8s(REG_DIG_H6)
        h5 = (h5 << 4)
        self.dig_h5 = h5 | (self.i2c_bus.read_reg_8u(REG_DIG_H5) >> 4 & 0x0F)

    def _read_raw_temp(self):
        while self.i2c_bus.read_reg_8u(REG_STATUS) & 0x08:
            time.sleep(.002)
        data = self.i2c_bus.read_reg_list(REG_TEMP_DATA, 3)
        return ((data[0] << 16) | (data[1] << 8) | data[2]) >> 4

    def _read_raw_pressure(self):
        while self.i2c_bus.read_reg_8u(REG_STATUS) & 0x08:
            time.sleep(.002)
        data = self.i2c_bus.read_reg_list(REG_PRESS_DATA, 3)
        return ((data[0] << 16) | (data[1] << 8) | data[2]) >> 4

    def _read_raw_humidity(self):
        while self.i2c_bus.read_reg_8u(REG_STATUS) & 0x08:
            time.sleep(.002)
        data = self.i2c_bus.read_reg_list(REG_HUM_DATA, 2)
        return (data[0] << 8) | data[1]

    def read(self):
        adc_t = float(self._read_raw_temp())
        var1 = (adc_t / 16384. - float(self.dig_t1) / 1024.) * float(self.dig_t2)
        var2 = ((adc_t / 131072. - float(self.dig_t1) / 8192.) * (
                adc_t / 131072. - float(self.dig_t1) / 8192.)) * float(self.dig_t3)
        self.t_fine = int(var1 + var2)
        temperature = (var1 + var2) / 5120.

        adc_p = float(self._read_raw_pressure())
        var1 = float(self.t_fine) / 2. - 64000.
        var2 = var1 * var1 * float(self.dig_p6) / 32768.
        var2 = var2 + var1 * float(self.dig_p5) * 2.
        var2 = var2 / 4. + float(self.dig_p4) * 65536.
        var1 = (float(self.dig_p3) * var1 * var1 / 524288. + float(self.dig_p2) * var1) / 524288.
        var1 = (1. + var1 / 32768.) * float(self.dig_p1)
        if var1 == 0:
            pressure = 0
        else:
            var_p = 1048576. - adc_p
            var_p = ((var_p - var2 / 4096.) * 6250.) / var1
            var1 = float(self.dig_p9) * var_p * var_p / 2147483648.
            var2 = var_p * float(self.dig_p8) / 32768.
            pressure = var_p + (var1 + var2 + float(self.dig_p7)) / 16.
            pressure /= 100  # hPa

        adc_h = float(self._read_raw_humidity())
        var_h = float(self.t_fine) - 76800.
        var_h = (adc_h - (float(self.dig_h4) * 64. + float(self.dig_h5) / 16384. * var_h)) * (
                float(self.dig_h2) / 65536. * (1. + float(self.dig_h6) / 67108864. * var_h * (
                1. + float(self.dig_h3) / 67108864. * var_h)))
        humidity = var_h * (1. - float(self.dig_h1) * var_h / 524288.)
        if humidity > 100:
            humidity = 100
        elif humidity < 0:
            humidity = 0

        return temperature, pressure, humidity
