# -*- coding: utf-8 -*-
#
# Moduł z czujnikiem koloru.
#  Dexter Industries Light & Color Sensor
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time

from modules import tcs34725


class LightColor(object):
    INTEGRATIONTIME_2_4MS = 0xFF
    INTEGRATIONTIME_24MS = 0xF6
    INTEGRATIONTIME_50MS = 0xEB
    INTEGRATIONTIME_101MS = 0xD5
    INTEGRATIONTIME_154MS = 0xC0
    INTEGRATIONTIME_700MS = 0x00

    GAIN_1X = 0x00
    GAIN_4X = 0x01
    GAIN_16X = 0x02
    GAIN_60X = 0x03

    def __init__(self, bus='I2C', integration_time=INTEGRATIONTIME_2_4MS, gain=GAIN_16X, led_state=False):
        self.tcs34725 = tcs34725.TCS34725(bus, integration_time=integration_time, gain=gain)
        self.set_led(led_state)

    def set_led(self, state):
        """ Stan diody (zapalona/zgaszona)

        state: True/False
        """
        self.tcs34725.set_interrupt(state)
        time.sleep(tcs34725.integration_time_delay[self.tcs34725.integration_time])

    def read_raw_color(self):
        """ Składowe R, G, B, C koloru """
        return self.tcs34725.get_raw_data()

    def read_rgb(self):
        """ Kolor RGB w formacie 8-bitowym """
        color = self.read_raw_color()
        r, g, b, c = list(map(lambda c: int(c * 255 / color[3]), color))
        return r, g, b
