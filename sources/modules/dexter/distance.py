# -*- coding: utf-8 -*-
#
# Moduł z czujnikiem odległości.
#  Dexter Industries Distance Sensor
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import vl53l0x


class Distance(object):
    def __init__(self, bus='I2C'):
        self.vl53l0x = vl53l0x.VL53L0X(bus=bus)
        # Zakres pomiaru 2 m
        self.vl53l0x.set_signal_rate_limit(.1)
        self.vl53l0x.set_vcsel_pulse_period(self.vl53l0x.VCSEL_PERIOD_PRE_RANGE, 18)
        self.vl53l0x.set_vcsel_pulse_period(self.vl53l0x.VCSEL_PERIOD_FINAL_RANGE, 14)

    def start_continuous(self, period_ms = 0):
        """ Rozpoczęcie pomiaru """
        self.vl53l0x.start_continuous(period_ms)

    def read_range_continuous(self):
        """ Odległość w mm """
        return self.vl53l0x.read_range_continuous_millimeters()

    def read_range_single(self, safe_infinity=True):
        """ Odległość w mm (pojedynczy pomiar o mniejszej precyzji) """
        value = self.vl53l0x.read_range_single_millimeters()
        # Zdarza się, że czujnik zwróci małą wartość, kiedy powinien odczytać nieskończoność
        if safe_infinity and value < 8190:
            for i in range(3):
                value = self.vl53l0x.read_range_single_millimeters()
                if value >= 8190:
                    return value
        return value
