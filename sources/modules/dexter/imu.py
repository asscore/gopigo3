# -*- coding: utf-8 -*-
#
# Moduł z 3-osiowym akcelerometrem, żyroskopem i magnetometrem.
#  Dexter Industries Inertial Measurement Unit Sensor
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import math

from modules import bno055


class IMU(object):
    def __init__(self, bus='I2C'):
        self.bno055 = bno055.BNO055(bus=bus)

    def read_euler(self):
        """ Kąty Eulera w stopniach """
        return self.bno055.read_euler()

    def read_magnetometer(self):
        """ Stan kompasu w uT """
        return self.bno055.read_magnetometer()

    def read_gyroscope(self):
        """ Stan żyroskopu w stopnie/s """
        return self.bno055.read_gyroscope()

    def read_accelerometer(self):
        """ Stan akcelerometru w m/s^2 """
        return self.bno055.read_accelerometer()

    def read_linear_acceleration(self):
        """ Przyspieszenie liniowe w m/s^2 """
        return self.bno055.read_linear_acceleration()

    def read_gravity(self):
        """ Przyspieszenie grawitacyjne w m/s^2 """
        return self.bno055.read_gravity()

    def read_quaternion(self):
        """ Kwaterniony """
        return self.bno055.read_quaternion()

    def read_temperature(self):
        """ Temperatura w C """
        return self.bno055.read_temperature()

    def get_north_point(self, magnetic_declination=5.83):
        """ Położenie względem północy w stopniach """
        x, y, z = self.read_magnetometer()

        # Czujnik zamontowany z przodu robota, układem do wnętrza
        heading = -math.atan2(x, -z) * 180 / math.pi

        if heading < 0:
            heading += 360
        elif heading > 360:
            heading -= 360

        # Wartość ujemna robot skierowany na zachód, wartość dodatnia na wschód (0 stopni północ)
        if 180 < heading <= 360:
            heading -= 360

        # http://www.magnetic-declination.com/
        heading += magnetic_declination  # Domyślnie +5 50 (Stalowa Wola, Polska)
        return heading
