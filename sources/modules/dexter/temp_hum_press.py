# -*- coding: utf-8 -*-
#
# Moduł z czujnikiem temperatury, wilgotności i ciśnienia.
#  Dexter Industries Temperature Humidity & Pressure Sensor
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

from modules import bme280


class TempHumPress(object):
    def __init__(self, bus='I2C', address=0x76):
        self.bme280 = bme280.BME280(bus, address, bme280.OSAMPLE_2, bme280.OSAMPLE_4, bme280.OSAMPLE_4,
                                    bme280.STANDBY_10, bme280.FILTER_8)

    def read(self):
        """ Temperatura w C, ciśnienie w hPa, wilgotność w % """
        return self.bme280.read()
