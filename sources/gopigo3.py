# -*- coding: utf-8 -*-
#
# Biblioteka do komunikacji z nakładką GoPiGo3.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import math
import spidev
import subprocess
import time

FIRMWARE_VERSION_REQUIRED = '1.0.x'

gpg_spi = spidev.SpiDev()
gpg_spi.open(0, 1)
gpg_spi.max_speed_hz = 500000
gpg_spi.mode = 0b00
gpg_spi.bits_per_word = 8


def spi_transfer_array(data_out):
    return gpg_spi.xfer2(data_out)


class Enumeration(object):
    def __init__(self, names):
        number = 0
        for line, name in enumerate(names.split('\n')):
            if name.find(',') >= 0:
                while name.find(' ') != -1:
                    name = name[:name.find(' ')] + name[(name.find(' ') + 1):]

                while name.find(',') != -1:
                    name = name[:name.find(',')] + name[(name.find(',') + 1):]

                if name.find('=') != -1:
                    number = int(float(name[(name.find('=') + 1):]))
                    name = name[:name.find('=')]

                setattr(self, name, number)
                number = number + 1


class FirmwareVersionError(Exception):
    """ Niezgodność wersji oprogramowania """


class SensorError(Exception):
    """ Błąd odczytu z czujnika """


class I2CError(Exception):
    """ Błąd operacji I2C """


class ValueError(Exception):
    """ Nieprawidłowa wartość """


class GoPiGo3(object):
    WHEEL_BASE_WIDTH = 117  # Odległość w mm od lewego do prawego koła
    WHEEL_DIAMETER = 66.5  # Średnica koła w mm
    WHEEL_BASE_CIRCUMFERENCE = WHEEL_BASE_WIDTH * math.pi  # Obwód koła w mm
    WHEEL_CIRCUMFERENCE = WHEEL_DIAMETER * math.pi  # Obwód koła w mm
    MOTOR_GEAR_RATIO = 120  # Przekładnia 120:1
    ENCODER_TICKS_PER_ROTATION = 6  # Liczba impulsów na obrót
    MOTOR_TICKS_PER_DEGREE = ((MOTOR_GEAR_RATIO * ENCODER_TICKS_PER_ROTATION) / 360.)  # Liczba impulsów na stopień

    GROVE_I2C_LENGTH_LIMIT = 32

    SERVO_PULSE_WIDTH_RANGE = 1850  # Zakres szerokości impulsu (od 0 do 180 stopni)

    SPI_MESSAGE_TYPE = Enumeration('''
        NONE,
        GET_MANUFACTURER,
        GET_NAME,
        GET_HARDWARE_VERSION,
        GET_FIRMWARE_VERSION,
        GET_ID,
        SET_LED,
        GET_VOLTAGE_5V,
        GET_VOLTAGE_VCC,
        SET_SERVO,
        SET_MOTOR_PWM,
        SET_MOTOR_POSITION,
        SET_MOTOR_POSITION_KP,
        SET_MOTOR_POSITION_KD,
        SET_MOTOR_DPS,
        SET_MOTOR_LIMITS,
        OFFSET_MOTOR_ENCODER,
        GET_MOTOR_ENCODER_LEFT,
        GET_MOTOR_ENCODER_RIGHT,
        GET_MOTOR_STATUS_LEFT,
        GET_MOTOR_STATUS_RIGHT,
        SET_GROVE_TYPE,
        SET_GROVE_MODE,
        SET_GROVE_STATE,
        SET_GROVE_PWM_DUTY,
        SET_GROVE_PWM_FREQUENCY,
        GET_GROVE_VALUE_1,
        GET_GROVE_VALUE_2,
        GET_GROVE_STATE_1_1,
        GET_GROVE_STATE_1_2,
        GET_GROVE_STATE_2_1,
        GET_GROVE_STATE_2_2,
        GET_GROVE_VOLTAGE_1_1,
        GET_GROVE_VOLTAGE_1_2,
        GET_GROVE_VOLTAGE_2_1,
        GET_GROVE_VOLTAGE_2_2,
        GET_GROVE_ANALOG_1_1,
        GET_GROVE_ANALOG_1_2,
        GET_GROVE_ANALOG_2_1,
        GET_GROVE_ANALOG_2_2,
        START_GROVE_I2C_1,
        START_GROVE_I2C_2,
    ''')

    GROVE_TYPE = Enumeration('''
        CUSTOM = 1,
        IR_DI_REMOTE,
        IR_EV3_REMOTE,
        US,
        I2C,
    ''')

    GROVE_MODE = Enumeration('''
        INPUT_DIGITAL = 0,
        OUTPUT_DIGITAL,
        INPUT_DIGITAL_PULLUP,
        INPUT_DIGITAL_PULLDOWN,
        INPUT_ANALOG,
        OUTPUT_PWM,
        INPUT_ANALOG_PULLUP,
        INPUT_ANALOG_PULLDOWN,
    ''')

    GROVE_STATE = Enumeration('''
        VALID_DATA,
        NOT_CONFIGURED,
        CONFIGURING,
        NO_DATA,
        I2C_ERROR,
    ''')

    LED_EYE_LEFT = 0x01
    LED_EYE_RIGHT = 0x02
    LED_LEFT = 0x04
    LED_RIGHT = 0x08
    LED_WIFI = 0x80

    SERVO_1 = 0x01
    SERVO_2 = 0x02

    MOTOR_LEFT = 0x01
    MOTOR_RIGHT = 0x02

    MOTOR_FLOAT = -128

    GROVE_AD1_1 = 0x01
    GROVE_AD1_2 = 0x02
    GROVE_AD2_1 = 0x04
    GROVE_AD2_2 = 0x08

    GROVE_AD1 = GROVE_AD1_1 + GROVE_AD1_2
    GROVE_AD2 = GROVE_AD2_1 + GROVE_AD2_2

    grove_type = [0, 0]
    grove_i2c_in_bytes = [0, 0]

    LOW = 0
    HIGH = 1

    def __init__(self, addr=8, detect=True):
        subprocess.call('gpio mode 12 ALT0', shell=True)
        subprocess.call('gpio mode 13 ALT0', shell=True)
        subprocess.call('gpio mode 14 ALT0', shell=True)

        self.spi_address = addr
        if detect:
            try:
                manufacturer = self.get_manufacturer()
                board = self.get_board()
                vfw = self.get_version_firmware()
            except IOError:
                raise IOError('No SPI response. GoPiGo3 with address %d not connected.' % addr)
            if manufacturer != 'Dexter Industries' or board != 'GoPiGo3':
                raise IOError('GoPiGo3 with address %d not connected.' % addr)
            if vfw.split('.')[0] != FIRMWARE_VERSION_REQUIRED.split('.')[0] or \
                    vfw.split('.')[1] != FIRMWARE_VERSION_REQUIRED.split('.')[1]:
                raise FirmwareVersionError('GoPiGo3 firmware needs to be version %s but is currently version %s.' \
                                           % (FIRMWARE_VERSION_REQUIRED, vfw))

    def spi_read_8(self, message_type):
        out_array = [self.spi_address, message_type, 0, 0, 0]
        reply = spi_transfer_array(out_array)
        if reply[3] == 0xA5:
            return int(reply[4])
        raise IOError('No SPI response.')

    def spi_read_16(self, message_type):
        out_array = [self.spi_address, message_type, 0, 0, 0, 0]
        reply = spi_transfer_array(out_array)
        if reply[3] == 0xA5:
            return int((reply[4] << 8) | reply[5])
        raise IOError('No SPI response.')

    def spi_read_32(self, message_type):
        out_array = [self.spi_address, message_type, 0, 0, 0, 0, 0, 0]
        reply = spi_transfer_array(out_array)
        if reply[3] == 0xA5:
            return int((reply[4] << 24) | (reply[5] << 16) | (reply[6] << 8) | reply[7])
        raise IOError('No SPI response.')

    def spi_write_32(self, message_type, value):
        out_array = [self.spi_address, message_type, ((value >> 24) & 0xFF), ((value >> 16) & 0xFF),
                     ((value >> 8) & 0xFF), (value & 0xFF)]
        spi_transfer_array(out_array)

    def get_manufacturer(self):
        """ Nazwa producenta nakładki """
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.GET_MANUFACTURER,
                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        reply = spi_transfer_array(out_array)
        if reply[3] == 0xA5:
            name = ''
            for c in range(4, 24):
                if reply[c] != 0:
                    name += chr(reply[c])
                else:
                    break
            return name
        raise IOError('No SPI response.')

    def get_board(self):
        """ Nazwa nakładki """
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.GET_NAME,
                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        reply = spi_transfer_array(out_array)
        if reply[3] == 0xA5:
            name = ''
            for c in range(4, 24):
                if reply[c] != 0:
                    name += chr(reply[c])
                else:
                    break
            return name
        raise IOError('No SPI response.')

    def get_version_hardware(self):
        """ Wersja nakładki """
        version = self.spi_read_32(self.SPI_MESSAGE_TYPE.GET_HARDWARE_VERSION)
        return '%d.x.x' % (version / 1000000)

    def get_version_firmware(self):
        """ Wersja oprogramowania """
        version = self.spi_read_32(self.SPI_MESSAGE_TYPE.GET_FIRMWARE_VERSION)
        return '%d.%d.%d' % ((version / 1000000), ((version / 1000) % 1000), (version % 1000))

    def get_id(self):
        """ Numer seryjny """
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.GET_ID,
                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        reply = spi_transfer_array(out_array)
        if reply[3] == 0xA5:
            return '%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X' % \
                   (reply[4], reply[5], reply[6], reply[7], reply[8], reply[9], reply[10], reply[11], reply[12],
                    reply[13], reply[14], reply[15], reply[16], reply[17], reply[18], reply[19])
        raise IOError('No SPI response.')

    def set_led(self, led, r, g=0, b=0):
        """ Kolor diody LED

        led: LED_xxx
        """
        if led < 0 or led > 255:
            return

        if r > 255:
            r = 255
        if g > 255:
            g = 255
        if b > 255:
            b = 255

        if r < 0:
            r = 0
        if g < 0:
            g = 0
        if b < 0:
            b = 0

        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.SET_LED, led, r, g, b]
        spi_transfer_array(out_array)

    def get_voltage_5v(self):
        """ Poziom napięcia obwodu 5V """
        value = self.spi_read_16(self.SPI_MESSAGE_TYPE.GET_VOLTAGE_5V)
        return value / 1000.

    def get_voltage_battery(self):
        """ Poziom naładowania baterii w V """
        value = self.spi_read_16(self.SPI_MESSAGE_TYPE.GET_VOLTAGE_VCC)
        return value / 1000.

    def set_servo(self, servo, us):
        """ Pozycja serwomechanizmu w us

        servo: SERVO_x
        us: 0 - 16666

        Szerokość impulsu dla pozycji w stopniach:
          (1375 - (SERVO_PULSE_WIDTH_RANGE / 2)) + ((SERVO_PULSE_WIDTH_RANGE / 180) * pozycja)
        """
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.SET_SERVO, servo, ((us >> 8) & 0xFF), (us & 0xFF)]
        spi_transfer_array(out_array)

    def set_motor_power(self, port, power):
        """ Moc silnika w %

        port: MOTOR_LEFT i/lub MOTOR_RIGHT
        power: -100 - 100 lub MOTOR_FLOAT
        """
        if power > 127:
            power = 127
        if power < -128:
            power = -128
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.SET_MOTOR_PWM, port, int(power)]
        spi_transfer_array(out_array)

    def set_motor_position(self, port, position):
        """ Pozycja silnika w stopniach

        port: MOTOR_LEFT i/lub MOTOR_RIGHT
        position: -360 - 360
        """
        position_raw = int(position * self.MOTOR_TICKS_PER_DEGREE)
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.SET_MOTOR_POSITION, port, ((position_raw >> 24) & 0xFF),
                     ((position_raw >> 16) & 0xFF), ((position_raw >> 8) & 0xFF), (position_raw & 0xFF)]
        spi_transfer_array(out_array)

    def set_motor_dps(self, port, dps):
        """ Prędkość silnika w stopnie/s

        port: MOTOR_LEFT i/lub MOTOR_RIGHT
        dps: stopnie na sekundę
        """
        dps = int(dps * self.MOTOR_TICKS_PER_DEGREE)
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.SET_MOTOR_DPS, int(port), ((dps >> 8) & 0xFF),
                     (dps & 0xFF)]
        spi_transfer_array(out_array)

    def set_motor_limits(self, port, power=0, dps=0):
        """ Limit prędkości silnika (0 brak limitu)

        port: MOTOR_LEFT i/lub MOTOR_RIGHT
        power: 0 - 100 lub MOTOR_FLOAT 
        dps: stopnie na sekundę
        """
        dps = int(dps * self.MOTOR_TICKS_PER_DEGREE)
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.SET_MOTOR_LIMITS, int(port), int(power),
                     ((dps >> 8) & 0xFF), (dps & 0xFF)]
        spi_transfer_array(out_array)

    def get_motor_status(self, port):
        """ Status silnika (flagi, moc, pozycja enkodera, prędkość)

        port: MOTOR_LEFT/MOTOR_RIGHT
        """
        if port == self.MOTOR_LEFT:
            message_type = self.SPI_MESSAGE_TYPE.GET_MOTOR_STATUS_LEFT
        elif port == self.MOTOR_RIGHT:
            message_type = self.SPI_MESSAGE_TYPE.GET_MOTOR_STATUS_RIGHT
        else:
            raise IOError('Must be one motor port at a time. MOTOR_LEFT or MOTOR_RIGHT.')

        out_array = [self.spi_address, message_type, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        reply = spi_transfer_array(out_array)
        if reply[3] == 0xA5:
            power = int(reply[5])
            if power & 0x80:
                power = power - 0x100

            encoder = int((reply[6] << 24) | (reply[7] << 16) | (reply[8] << 8) | reply[9])
            if encoder & 0x80000000:
                encoder = int(encoder - 0x100000000)

            dps = int((reply[10] << 8) | reply[11])
            if dps & 0x8000:
                dps = dps - 0x10000

            return reply[4], power, int(encoder / self.MOTOR_TICKS_PER_DEGREE), int(dps / self.MOTOR_TICKS_PER_DEGREE)
        raise IOError('No SPI response.')

    def get_motor_encoder(self, port):
        """ Bieżąca pozycja enkodera w stopniach

        port: MOTOR_LEFT/MOTOR_RIGHT
        """
        if port == self.MOTOR_LEFT:
            message_type = self.SPI_MESSAGE_TYPE.GET_MOTOR_ENCODER_LEFT
        elif port == self.MOTOR_RIGHT:
            message_type = self.SPI_MESSAGE_TYPE.GET_MOTOR_ENCODER_RIGHT
        else:
            raise IOError('Port(s) unsupported. Must be one at a time.')

        encoder = self.spi_read_32(message_type)
        if encoder & 0x80000000:
            encoder = int(encoder - 0x100000000)
        return int(encoder / self.MOTOR_TICKS_PER_DEGREE)

    def offset_motor_encoder(self, port, offset):
        """ Przesunięcie pozycji enkodera

        port: MOTOR_LEFT i/lub MOTOR_RIGHT
        offset: przesunięcie
        """
        offset = int(offset * self.MOTOR_TICKS_PER_DEGREE)
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.OFFSET_MOTOR_ENCODER, int(port), ((offset >> 24) & 0xFF),
                     ((offset >> 16) & 0xFF), ((offset >> 8) & 0xFF), (offset & 0xFF)]
        spi_transfer_array(out_array)

    def set_grove_type(self, port, type):
        """ Sposób obsługi portu Grove

        port: GROVE_ADx
        type: GROVE_TYPE.xxx
        """
        for p in range(2):
            if ((port >> (p * 2)) & 3) == 3:
                self.grove_type[p] = type
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.SET_GROVE_TYPE, port, type]
        spi_transfer_array(out_array)

    def set_grove_mode(self, pin, mode):
        """ Tryb pracy pinu Grove

        pin: GROVE_ADx_x
        mode: GROVE_MODE.xxx
        """
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.SET_GROVE_MODE, pin, mode]
        spi_transfer_array(out_array)
        time.sleep(.005)

    def set_grove_state(self, pin, state):
        """ Stan pinu Grove

        pin: GROVE_ADx_x
        state: LOW/HIGH
        """
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.SET_GROVE_STATE, pin, state]
        spi_transfer_array(out_array)

    def set_grove_pwm_duty(self, pin, duty):
        """ Wypełnienia sygnału PWM

        pin: GROVE_ADx_x
        duty: wypełnienie w procentach
        """
        if duty < 0:
            duty = 0
        if duty > 100:
            duty = 100
        duty_value = int(duty * 10.)
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.SET_GROVE_PWM_DUTY, pin, ((duty_value >> 8) & 0xFF),
                     (duty_value & 0xFF)]
        spi_transfer_array(out_array)

    def set_grove_pwm_frequency(self, pin, freq=24000):
        """ Częstotliwość sygnału PWM w Hz

        pin: GROVE_ADx_x
        freq: 3 - 48000 Hz
        """
        if freq < 3:
            freq = 3
        if freq > 48000:
            freq = 48000
        freq_value = int(freq)
        out_array = [self.spi_address, self.SPI_MESSAGE_TYPE.SET_GROVE_PWM_FREQUENCY, pin, ((freq_value >> 8) & 0xFF),
                     (freq_value & 0xFF)]
        spi_transfer_array(out_array)

    def grove_i2c_transfer(self, port, addr, out_arr, in_bytes=0):
        """ Operacja I2C

        port: GROVE_ADx
        addr: adres
        out_arr: bajty do wysłania
        in_bytes: liczba bajtów do odczytania
        """
        timeout = time.time() + .005
        start = False
        while not start:
            try:
                self.grove_i2c_start(port, addr, out_arr, in_bytes)
                start = True
            except (IOError, I2CError):
                if time.time() > timeout:
                    raise IOError('Timeout trying to start transaction.')

        delay_time = 0
        if len(out_arr):
            delay_time += 1 + len(out_arr)
        if in_bytes:
            delay_time += 1 + in_bytes
        delay_time *= .000115
        time.sleep(delay_time)

        timeout = time.time() + .005
        while True:
            try:
                values = self.get_grove_value(port)
                return values
            except (ValueError, SensorError):
                if time.time() > timeout:
                    raise IOError('Timeout waiting for data.')

    def grove_i2c_start(self, port, addr, out_arr, in_bytes=0):
        """ Start operacji I2C

        port: GROVE_ADx
        addr: adres
        out_arr: bajty do wysłania
        in_bytes: liczba bajtów do odczytania
        """
        if port == self.GROVE_AD1:
            message_type = self.SPI_MESSAGE_TYPE.START_GROVE_I2C_1
            port_index = 0
        elif port == self.GROVE_AD2:
            message_type = self.SPI_MESSAGE_TYPE.START_GROVE_I2C_2
            port_index = 1
        else:
            raise RuntimeError('Port unsupported. Must get one at a time.')

        address = ((int(addr) & 0x7F) << 1)

        if in_bytes > self.GROVE_I2C_LENGTH_LIMIT:
            raise RuntimeError('Read length error. Up to %d bytes can be read in a single transaction.' \
                               % self.GROVE_I2C_LENGTH_LIMIT)

        out_bytes = len(out_arr)
        if out_bytes > self.GROVE_I2C_LENGTH_LIMIT:
            raise RuntimeError('Write length error. Up to %d bytes can be written in a single transaction.' \
                               % self.GROVE_I2C_LENGTH_LIMIT)

        out_array = [self.spi_address, message_type, address, in_bytes, out_bytes]
        out_array.extend(out_arr)
        reply = spi_transfer_array(out_array)
        self.grove_i2c_in_bytes[port_index] = in_bytes
        if reply[3] != 0xA5:
            raise IOError('No SPI response.')
        if reply[4] != 0:
            raise I2CError('Not ready to start I2C transaction.')

    def get_grove_value(self, port):
        """ Wartość na porcie Grove (odczyt w zależności od ustawionego sposobu obsługi)

        port: GROVE_ADx
        """
        if port == self.GROVE_AD1:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_VALUE_1
            port_index = 0
        elif port == self.GROVE_AD2:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_VALUE_2
            port_index = 1
        else:
            raise IOError('Port unsupported. Must get one at a time.')

        if self.grove_type[port_index] == self.GROVE_TYPE.IR_DI_REMOTE:
            out_array = [self.spi_address, message_type, 0, 0, 0, 0, 0]
            reply = spi_transfer_array(out_array)
            if reply[3] == 0xA5:
                if reply[4] == self.grove_type[port_index] and reply[5] == 0:
                    return reply[6]
                else:
                    raise SensorError('Invalid value.')
            else:
                raise IOError('No SPI response.')
        elif self.grove_type[port_index] == self.GROVE_TYPE.IR_EV3_REMOTE:
            out_array = [self.spi_address, message_type, 0, 0, 0, 0, 0, 0, 0, 0]
            reply = spi_transfer_array(out_array)
            if reply[3] == 0xA5:
                if reply[4] == self.grove_type[port_index] and reply[5] == 0:
                    return [reply[6], reply[7], reply[8], reply[9]]
                else:
                    raise SensorError('Invalid value.')
            else:
                raise IOError('No SPI response.')
        elif self.grove_type[port_index] == self.GROVE_TYPE.US:
            out_array = [self.spi_address, message_type, 0, 0, 0, 0, 0, 0]
            reply = spi_transfer_array(out_array)
            if reply[3] == 0xA5:
                if reply[4] == self.grove_type[port_index] and reply[5] == 0:
                    value = (((reply[6] << 8) & 0xFF00) | (reply[7] & 0xFF))
                    if value == 0:
                        raise SensorError('Sensor not responding.')
                    elif value == 1:
                        raise ValueError('Object not detected within range.')
                    else:
                        return value
                else:
                    raise SensorError('Invalid value.')
            else:
                raise IOError('No SPI response.')
        elif self.grove_type[port_index] == self.GROVE_TYPE.I2C:
            out_array = [self.spi_address, message_type, 0, 0, 0, 0]
            out_array.extend([0 for _ in range(self.grove_i2c_in_bytes[port_index])])
            reply = spi_transfer_array(out_array)
            if reply[3] == 0xA5:
                if reply[4] == self.grove_type[port_index]:
                    if reply[5] == self.GROVE_STATE.VALID_DATA:
                        return reply[6:]
                    elif reply[5] == self.GROVE_STATE.I2C_ERROR:
                        raise I2CError('I2C bus error.')
                    else:
                        raise ValueError('Invalid value.')
                else:
                    raise SensorError('Grove type mismatch.')
            else:
                raise IOError('No SPI response.')
        return self.spi_read_8(message_type)

    def get_grove_state(self, pin):
        """ Stan pinu Grove

        pin: GROVE_ADx_x
        """
        if pin == self.GROVE_AD1_1:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_STATE_1_1
        elif pin == self.GROVE_AD1_2:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_STATE_1_2
        elif pin == self.GROVE_AD2_1:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_STATE_2_1
        elif pin == self.GROVE_AD2_2:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_STATE_2_2
        else:
            raise IOError('Pin(s) unsupported. Must get one at a time.')

        out_array = [self.spi_address, message_type, 0, 0, 0, 0]
        reply = spi_transfer_array(out_array)
        if reply[3] == 0xA5:
            if reply[4] == self.GROVE_STATE.VALID_DATA:
                return reply[5]
            else:
                raise ValueError('Invalid value.')
        else:
            raise IOError('No SPI response.')

    def get_grove_voltage(self, pin):
        """ Stan pinu Grove w V

        pin: GROVE_ADx_x
        """
        if pin == self.GROVE_AD1_1:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_VOLTAGE_1_1
        elif pin == self.GROVE_AD1_2:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_VOLTAGE_1_2
        elif pin == self.GROVE_AD2_1:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_VOLTAGE_2_1
        elif pin == self.GROVE_AD2_2:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_VOLTAGE_2_2
        else:
            raise IOError('Pin(s) unsupported. Must get one at a time.')

        out_array = [self.spi_address, message_type, 0, 0, 0, 0, 0]
        reply = spi_transfer_array(out_array)
        if reply[3] == 0xA5:
            if reply[4] == self.GROVE_STATE.VALID_DATA:
                return (((reply[5] << 8) & 0xFF00) | (reply[6] & 0xFF)) / 1000.
            else:
                raise ValueError('Invalid value.')
        else:
            raise IOError('No SPI response.')

    def get_grove_analog(self, pin):
        """ Wartość (12-bitowa) na pinie Grove

        port: GROVE_ADx_x
        """
        if pin == self.GROVE_AD1_1:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_ANALOG_1_1
        elif pin == self.GROVE_AD1_2:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_ANALOG_1_2
        elif pin == self.GROVE_AD2_1:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_ANALOG_2_1
        elif pin == self.GROVE_AD2_2:
            message_type = self.SPI_MESSAGE_TYPE.GET_GROVE_ANALOG_2_2
        else:
            raise IOError('Pin(s) unsupported. Must get one at a time.')

        out_array = [self.spi_address, message_type, 0, 0, 0, 0, 0]
        reply = spi_transfer_array(out_array)
        if reply[3] == 0xA5:
            if reply[4] == self.GROVE_STATE.VALID_DATA:
                return ((reply[5] << 8) & 0xFF00) | (reply[6] & 0xFF)
            else:
                raise ValueError('Invalid value.')
        else:
            raise IOError('No SPI response.')

    def reset(self):
        """ Przywrócenie stanu początkowego """
        self.set_grove_type(self.GROVE_AD1 + self.GROVE_AD2, self.GROVE_TYPE.CUSTOM)
        self.set_grove_mode(self.GROVE_AD1 + self.GROVE_AD2, self.GROVE_MODE.INPUT_DIGITAL)
        self.set_motor_power(self.MOTOR_LEFT + self.MOTOR_RIGHT, self.MOTOR_FLOAT)
        self.set_motor_limits(self.MOTOR_LEFT + self.MOTOR_RIGHT, 0, 0)
        self.set_servo(self.SERVO_1 + self.SERVO_2, 0)
        self.set_led(self.LED_EYE_LEFT + self.LED_EYE_RIGHT + self.LED_LEFT + self.LED_RIGHT, 0, 0, 0)
