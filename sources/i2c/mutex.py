# -*- coding: utf-8 -*-
#
# Klasa pomocnicza do równoczesnego dostępu do interfejsu I2C.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import fcntl
import time


class Mutex(object):
    handle = None

    def __init__(self):
        pass

    def __enter__(self):
        self.acquire()
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.release()

    def __del__(self):
        pass

    def acquire(self):
        """ Dostępu do zasobu, jeśli zajęty oczekiwanie aż zostanie zwolniony """
        acquired = False
        while not acquired:
            try:
                self.handle = open('/run/lock/i2c.lock', 'w')
                fcntl.lockf(self.handle, fcntl.LOCK_EX | fcntl.LOCK_NB)
                acquired = True
            except IOError:
                time.sleep(.001)
            except Exception as e:
                print(e)

    def release(self):
        """ Zwolnienie zasobu """
        if self.handle is not None and self.handle is not True:
            self.handle.close()
            self.handle = None
            time.sleep(.001)
