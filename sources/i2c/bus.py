# -*- coding: utf-8 -*-
#
# Interfejs I2C.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import time


class Bus(object):
    def __init__(self, address, big_endian=True, bus='I2C', module='smbus'):
        if bus == 'I2C':
            self.bus_name = bus
            self.i2c_module = module

            if module == 'smbus':
                self.smbus_module = __import__('smbus')
                self.i2c_bus = self.smbus_module.SMBus(1)
            elif module == 'periphery':
                self.periphery_module = __import__('periphery')
                self.i2c_bus = self.periphery_module.I2C('/dev/i2c-1')
            else:
                raise IOError('I2C module not supported.')
        elif bus == 'AD1' or bus == 'AD2':
            self.bus_name = bus

            self.gopigo3_module = __import__('gopigo3')
            self.gpg3 = self.gopigo3_module.GoPiGo3()
            if bus == 'AD1':
                self.port = self.gpg3.GROVE_AD1
            elif bus == 'AD2':
                self.port = self.gpg3.GROVE_AD2
            self.gpg3.set_grove_type(self.port, self.gpg3.GROVE_TYPE.I2C)
            time.sleep(.01)
        else:
            raise IOError('I2C bus not supported.')

        self.address = address
        self.big_endian = big_endian

    def reconfig_bus(self):
        if self.bus_name == 'AD1' or self.bus_name == 'AD2':
            self.gpg3.set_grove_type(self.port, self.gpg3.GROVE_TYPE.I2C)

    def set_address(self, address):
        self.address = address

    def transfer(self, out_arr, in_bytes=0):
        if self.bus_name == 'I2C':
            if self.i2c_module == 'smbus':
                if len(out_arr) >= 2 and in_bytes == 0:
                    self.i2c_bus.write_i2c_block_data(self.address, out_arr[0], out_arr[1:])
                elif len(out_arr) == 1 and in_bytes == 0:
                    self.i2c_bus.write_byte(self.address, out_arr[0])
                elif len(out_arr) == 1 and in_bytes >= 1:
                    return self.i2c_bus.read_i2c_block_data(self.address, out_arr[0], in_bytes)
                elif len(out_arr) == 1 and in_bytes is None:
                    return self.i2c_bus.read_i2c_block_data(self.address, out_arr[0])
                elif len(out_arr) == 0 and in_bytes >= 1:
                    return self.i2c_bus.read_byte(self.address)
                else:
                    raise IOError('I2C operation not supported.')
            elif self.i2c_module == 'periphery':
                msgs = []
                offset = 0
                if in_bytes is None:
                    in_bytes = 32
                if len(out_arr) > 0:
                    msgs.append(self.i2c_bus.Message(out_arr))
                    offset = 1
                if in_bytes:
                    r = [0 for _ in range(in_bytes)]
                    msgs.append(self.i2c_bus.Message(r, read=True))
                if len(msgs) >= 1:
                    self.i2c_bus.transfer(self.address, msgs)
                if in_bytes:
                    return msgs[offset].data
                return
        elif self.bus_name == 'AD1' or self.bus_name == 'AD2':
            try:
                return self.gpg3.grove_i2c_transfer(self.port, self.address, out_arr, in_bytes)
            except self.gopigo3_module.I2CError:
                raise IOError("Input/output error.")

    def write_8(self, val):
        """ Zapis wartości 8-bitowej """
        val = int(val)
        self.transfer([val])

    def write_reg_8(self, reg, val):
        """ Zapis wartości 8-bitowej do rejestru """
        val = int(val)
        self.transfer([reg, val])

    def write_reg_16(self, reg, val, big_endian=None):
        """ Zapis wartości 16-bitowej do rejestru """
        val = int(val)
        if big_endian is None:
            big_endian = self.big_endian
        if big_endian:
            self.transfer([reg, ((val >> 8) & 0xFF), (val & 0xFF)])
        else:
            self.transfer([reg, (val & 0xFF), ((val >> 8) & 0xFF)])

    def write_reg_32(self, reg, val, big_endian=None):
        """ Zapis wartości 32-bitowej do rejestru """
        val = int(val)
        if big_endian is None:
            big_endian = self.big_endian
        if big_endian:
            self.transfer([reg, ((val >> 24) & 0xFF), ((val >> 16) & 0xFF), ((val >> 8) & 0xFF), (val & 0xFF)])
        else:
            self.transfer([reg, (val & 0xFF), ((val >> 8) & 0xFF), ((val >> 16) & 0xFF), ((val >> 24) & 0xFF)])

    def write_reg_list(self, reg, list):
        """ Zapis ciągu bajtów do rejestru """
        arr = [reg]
        arr.extend(list)
        self.transfer(arr)

    def read_8u(self):
        """ Odczyt wartości 8-bitowej """
        val = self.transfer([], 1)
        return val

    def read_8s(self):
        """ Odczyt wartości 8-bitowej ze znakiem """
        val = self.transfer([], 1)
        if val & 0x80:
            return val - 0x100
        return val

    def read_16u(self, big_endian=None):
        """ Odczyt wartości 16-bitowej """
        val = self.transfer([], 2)
        if big_endian is None:
            big_endian = self.big_endian
        if big_endian:
            return (val[0] << 8) | val[1]
        else:
            return (val[1] << 8) | val[0]

    def read_16s(self, big_endian=None):
        """ Odczyt wartości 16-bitowej ze znakiem """
        val = self.read_16u(big_endian)
        if val & 0x8000:
            return val - 0x10000
        return val

    def read_32u(self, big_endian=None):
        """ Odczyt wartości 32-bitowej """
        val = self.transfer([], 4)
        if big_endian is None:
            big_endian = self.big_endian
        if big_endian:
            return (val[0] << 24) | (val[1] << 16) | (val[2] << 8) | val[3]
        else:
            return (val[3] << 24) | (val[2] << 16) | (val[1] << 8) | val[0]

    def read_32s(self, big_endian=None):
        """ Odczyt wartości 32-bitowej ze znakiem """
        val = self.read_32u(big_endian)
        if val & 0x80000000:
            val = val - 0x100000000
        return val

    def read_list(self, len):
        """ Odczyt ciągu bajtów """
        return self.transfer([], len)

    def read_reg_8u(self, reg):
        """ Odczyt wartości 8-bitowej z rejestru """
        val = self.transfer([reg], 1)
        return val[0]

    def read_reg_8s(self, reg):
        """ Odczyt wartości 8-bitowej ze znakiem z rejestru """
        val = self.read_reg_8u(reg)
        if val & 0x80:
            return val - 0x100
        return val

    def read_reg_16u(self, reg, big_endian=None):
        """ Odczyt wartości 16-bitowej z rejestru """
        val = self.transfer([reg], 2)
        if big_endian is None:
            big_endian = self.big_endian
        if big_endian:
            return (val[0] << 8) | val[1]
        else:
            return (val[1] << 8) | val[0]

    def read_reg_16s(self, reg, big_endian=None):
        """ Odczyt wartości 16-bitowej ze znakiem z rejestru """
        val = self.read_reg_16u(reg, big_endian)
        if val & 0x8000:
            return val - 0x10000
        return val

    def read_reg_32u(self, reg, big_endian=None):
        """ Odczyt wartości 32-bitowej z rejestru """
        val = self.transfer([reg], 4)
        if big_endian is None:
            big_endian = self.big_endian
        if big_endian:
            return (val[0] << 24) | (val[1] << 16) | (val[2] << 8) | val[3]
        else:
            return (val[3] << 24) | (val[2] << 16) | (val[1] << 8) | val[0]

    def read_reg_32s(self, reg, big_endian=None):
        """ Odczyt wartości 32-bitowej ze znakiem z rejestru """
        val = self.read_reg_32u(reg, big_endian)
        if val & 0x80000000:
            val = val - 0x100000000
        return val

    def read_reg_list(self, reg, len=None):
        """ Odczyt ciągu bajtów z rejestru """
        return self.transfer([reg], len)
