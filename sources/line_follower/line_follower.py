# -*- coding: utf-8 -*-
#
# Biblioteka do odczytu stanu czujników IR z modułu Line Follower.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import os
import pickle

import numpy

import i2c

I2C_ADDR = 0x06

REGISTER = 0x01
COMMAND = 0x03


def statistical_noise_reduction(values, std_factor_threshold=2):
    if len(values) == 0:
        return []

    mean = numpy.mean(values)
    standard_deviation = numpy.std(values)

    if standard_deviation == 0:
        return values

    filtered_values = [element for element in values if element > mean - std_factor_threshold * standard_deviation]
    filtered_values = [element for element in filtered_values if
                       element < mean + std_factor_threshold * standard_deviation]
    return filtered_values


class LineFollower(object):
    MAX_BUFFER_LENGTH = 20
    sensor_buffer = [[], [], [], [], []]

    def __init__(self):
        self.i2c_bus = i2c.Bus(I2C_ADDR)
        self.path = os.path.dirname(__file__)
        self.white_line = self.get_white_line()
        self.black_line = self.get_black_line()
        self.threshold = [w + ((b - w) / 2) for w, b in zip(self.white_line, self.black_line)]

    def read_raw_sensors(self):
        """ Stan czujników (0 - 1023) """
        try:
            output_values = []
            self.i2c_bus.write_reg_list(REGISTER, [COMMAND, 0, 0, 0])

            number = self.i2c_bus.read_reg_list(REGISTER)
            number = self.i2c_bus.read_reg_list(REGISTER)

            for i in range(5):
                self.sensor_buffer[i].append(number[2 * i] * 256 + number[2 * i + 1])

                if len(self.sensor_buffer[i]) > self.MAX_BUFFER_LENGTH:
                    self.sensor_buffer[i].pop(0)

                filtered_value = statistical_noise_reduction(self.sensor_buffer[i], 2)[-1]

                output_values.append(filtered_value)
            return output_values
        except IOError:
            return 5 * [-1]

    def read(self):
        """ Stan czujników (1 - czarna linia, 0 - biała linia) """
        for i in range(5):
            vals = self.read_raw_sensors()
            if vals[0] != -1:
                break
        line_result = []
        for sensor_reading, cur_threshold in zip(vals, self.threshold):
            if sensor_reading > cur_threshold:
                line_result.append(1)
            else:
                line_result.append(0)
        return line_result

    def read_position(self):
        """ Położenie czarnej linii """
        vals = self.read()

        if vals == [0, 0, 1, 0, 0] or \
           vals == [0, 1, 1, 1, 0]:
            return 'center'
        if vals == [1, 1, 1, 1, 1]:
            return 'black'
        if vals == [0, 0, 0, 0, 0]:
            return 'white'
        if vals == [0, 1, 1, 0, 0] or \
           vals == [0, 1, 0, 0, 0] or \
           vals == [1, 0, 0, 0, 0] or \
           vals == [1, 1, 0, 0, 0] or \
           vals == [1, 1, 1, 0, 0] or \
           vals == [1, 1, 1, 1, 0]:
            return 'right'
        if vals == [0, 0, 0, 1, 0] or \
           vals == [0, 0, 1, 1, 0] or \
           vals == [0, 0, 0, 0, 1] or \
           vals == [0, 0, 0, 1, 1] or \
           vals == [0, 0, 1, 1, 1] or \
           vals == [0, 1, 1, 1, 1]:
            return 'left'
        return 'unknown'

    def set_black_line(self):
        self.black_line = self.read_raw_sensors()
        with open(self.path + '/black_line.txt', 'wb') as f:
            pickle.dump(self.black_line, f)

    def get_black_line(self):
        try:
            with open(self.path + '/black_line.txt', 'rb') as f:
                self.black_line = pickle.load(f)
        except:
            self.black_line = [0] * 5
        return self.black_line

    def set_white_line(self):
        self.white_line = self.read_raw_sensors()
        with open(self.path + '/white_line.txt', 'wb') as f:
            pickle.dump(self.white_line, f)

    def get_white_line(self):
        try:
            with open(self.path + '/white_line.txt', 'rb') as f:
                self.white_line = pickle.load(f)
        except:
            self.white_line = [0] * 5
        return self. white_line
