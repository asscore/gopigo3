# -*- coding: utf-8 -*-
#
# Usługa zarządzania energią.
#
# Copyright (c) 2018 ProMax/ASSCoRE. Wszelkie prawa zastrzeżone.
#

from __future__ import print_function
from __future__ import division

import os
import time

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

GPIO.setup(23, GPIO.OUT)  # Na postawie stanu tego portu nakładka wie, kiedy nastąpiło wyłączenie
GPIO.output(23, True)

while True:
    if GPIO.input(22):
        os.system('shutdown now -h')
    time.sleep(.1)
