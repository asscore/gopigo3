#!/bin/bash

unzip openocd_compiled.zip
sudo cp -rn openocd_compiled/files/openocd /usr/local/share
sudo cp -rn openocd_compiled/openocd /usr/local/bin
sudo chmod +x /usr/local/bin/openocd
rm -rf openocd_compiled
