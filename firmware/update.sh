#!/bin/bash

FW_UPDATE_FILE=$(find ./ -maxdepth 1 -name *.bin)

sudo openocd -f interface/raspberrypi2-native.cfg -c "transport select swd; set CHIPNAME at91samc20j18; source [find target/at91samdXX.cfg]; adapter_khz 250; adapter_nsrst_delay 100; adapter_nsrst_assert_width 100" -c "init; targets; reset halt; program $FW_UPDATE_FILE verify; reset" -c "shutdown"
